# Figure 4 slope figure
# Author: James Beattie

"""
This is the code for the 4th figure in my PVB304 report, which just compares
the power spectra slopes from my work with two other papers. 
"""

##########################################################################################

import numpy as np
import matplotlib.pyplot as plt
import matplotlib.colors as colors;
from mpl_toolkits.axes_grid1 import make_axes_locatable;
from matplotlib import rc, ticker
from mpl_toolkits.axes_grid1.inset_locator import inset_axes
import matplotlib.mlab as mlab

##########################################################################################

# My slopes
M1      =  -2.4376417066580953;
M4      = -2.2603274438522094;
M10     = -1.7253235192794831;
M20     = -1.5828240417462307;
M40     = -1.4861841475850628;
M100    =  -1.3305206725499557;

# My standard deviations of the slopes
MSD = 0.01;
M4D = 0.02;

# Federrath 2013
FedSlope = np.array( (-3.7, -2.1, -1.6, -1.1 ) )
FedMach = np.array( (3, 3, 10, 50) )

# Kim 2005
KimSlope = np.array( (-2.12, -2.24, -2.13, -1.14, -1.05) )
KimMach = np.array( (0.8, 1.7, 3.4, 7.5, 12.5) )

# My slopes
Slope = np.array( (M1,M4,M10,M20,M40,M100) )
Mach = np.array( (1,4,10,20,40,100) )
Err = np.array( (MSD, M4D, MSD, MSD, MSD, MSD) )



plt.plot(Mach,Slope,ls='-',color='red')
plt.plot(FedMach,FedSlope,ls='-',color='green')
plt.plot(Mach,Slope,'ro',label=r'$\Sigma(x,y)$: this study')
plt.plot(FedMach,FedSlope,'go',label=r'$\Sigma(x,y)$: Federrath and Klessen, 2013')
plt.plot(KimMach,KimSlope,ls='-',color='blue')
plt.plot(KimMach,KimSlope,'b*',label=r'$\rho(x,y,z)$: Kim and Ryu, 2005')
plt.xlabel(r'Mach Number',fontsize=16)
plt.ylabel(r'Slope',fontsize=16)
plt.legend()
plt.show()
