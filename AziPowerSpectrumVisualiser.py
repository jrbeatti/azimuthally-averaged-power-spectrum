# Averaging over the time domain visualisation
# Author: James Beattie
"""
This script takes the time averaged azimuthal data, finds the energy cascades,
performs least squares across the cascade and visualises the power spectra
using matplotlib
"""
##########################################################################################

import csv
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.colors as colors;
from mpl_toolkits.axes_grid1 import make_axes_locatable;
from matplotlib import rc, ticker
from mpl_toolkits.axes_grid1.inset_locator import inset_axes
import matplotlib.mlab as mlab
from itertools import chain
import re
from sklearn import datasets, linear_model
from matplotlib import rc, ticker
from sklearn.metrics import mean_squared_error, r2_score
import h5py;                                
from scipy import fftpack

##########################################################################################

def CSV_READER(csvfile):

    Means   = []
    SDs     = []
    counter = 1

    with open(csvfile,'rb') as CSV:

        readCSV = csv.reader(CSV, delimiter=',')

        for line in readCSV:


            Mean        = [float(line[1])]
            SD          = [float(line[2])]
            Means       = np.concatenate((Means,Mean))
            SDs         = np.concatenate((SDs,SD))
            Wavevector = range(1,len(Means)+1)

    return np.array(Means), np.array(SDs), np.array(Wavevector)

def h5py_reader(data):
    f            = h5py.File(data, 'r+');
    density      = np.array(f['dens_proj_xy'])
    time         = np.array(f['time']);
    f.close;
    return density, time

def linear_regression(X,Y,Xreal):

    Y      = np.transpose([np.log10(Y)]);
    X      = np.transpose([np.log10(X)]);

    # Perform the linear regression
    regr            = linear_model.LinearRegression();
    regr.fit(X,Y);
    log_log_predict = regr.predict(X);

    # Just make a bunch of values over the entire x domain for the line
    slope           = regr.coef_[0][0];
    intercept       = regr.intercept_[0];
    domain          = Xreal;
    abline_values   = [slope * i + intercept for i in domain];
    return slope, intercept, domain, abline_values, log_log_predict, Y

##########################################################################################

M4 = 10048**2;
M1 = 1024**2;
M10 = 1024**2;
M20 = 1024**2;
M40 = 1024**2;
M100 = 1024**2;

M_M1, SD_M1, WV_M1          = CSV_READER('Mach1Transformed.csv')
M_M4, SD_M4, WV_M4          = CSV_READER('Mach4Transformed.csv')
M_M10, SD_M10, WV_M10       = CSV_READER('Mach10Transformed.csv')
M_M20, SD_M20, WV_M20       = CSV_READER('Mach20Transformed.csv')
M_M40, SD_M40, WV_M40       = CSV_READER('Mach40Transformed.csv')
M_M100, SD_M100, WV_M100    = CSV_READER('Mach100Transformed.csv')

# Make 2 \pi k correction

M_M1    = M_M1*WV_M1
SD_M1   = SD_M1*WV_M1

M_M4    = M_M4*WV_M4
SD_M4   = SD_M4*WV_M4

M_M10   = M_M10*WV_M10
SD_M10  = SD_M10*WV_M10

M_M20   = M_M20*WV_M20
SD_M20  = SD_M20*WV_M20

M_M40   = M_M40*WV_M40
SD_M40  = SD_M40*WV_M40

M_M100  = M_M100*WV_M100
SD_M100 = SD_M100*WV_M100

# Important Scales

SonicScale          = 1/(0.0135);
UpperSubCascade     = 1/(0.008)
LowerSubCascade     = 1/(0.002)
UpperSuperCascade   = 1/(0.1)
LowerSuperCascade   = 1/(0.025)

# Linear Regressions
"""There will be 8 linear regressions in total, over each of the curves in the turbulence """

# Mach 4

SubCasM4    = M_M4[int(UpperSubCascade):int(LowerSubCascade)]
SubKM4      = WV_M4[int(UpperSubCascade):int(LowerSubCascade)]
SuperCasM4  = M_M4[int(UpperSuperCascade):int(LowerSuperCascade)]
SuperKM4    = WV_M4[int(UpperSuperCascade):int(LowerSuperCascade)]

# Other Mach Numbers

K           = WV_M1[int(UpperSuperCascade):int(LowerSuperCascade)]
CasM1       = M_M1[int(UpperSuperCascade):int(LowerSuperCascade)]
CasM10      = M_M10[int(UpperSuperCascade):int(LowerSuperCascade)]
CasM20      = M_M20[int(UpperSuperCascade):int(LowerSuperCascade)]
CasM40      = M_M40[int(UpperSuperCascade):int(LowerSuperCascade)]
CasM100     = M_M100[int(UpperSuperCascade):int(LowerSuperCascade)]

# All regressions

slopeSubM4, interceptSubM4, domainSubM4, abline_valuesSubM4, log_log_predictSubM4, YSubM4                           = linear_regression(SubKM4,SubCasM4,SubKM4)
slopeSuperM4, interceptSuperM4, domainSuperM4, abline_valuesSuperM4, log_log_predictSuperM4, YSuperM4               = linear_regression(SuperKM4,SuperCasM4,SuperKM4)
slopeSuperM1, interceptSuperM1, domainSuperM1, abline_valuesSuperM1, log_log_predictSuperM1, YSuperM1               = linear_regression(K,CasM1,K)
slopeSuperM10, interceptSuperM10, domainSuperM10, abline_valuesSuperM10, log_log_predictSuperM10, YSuperM10         = linear_regression(K,CasM10,K)
slopeSuperM20, interceptSuperM20, domainSuperM20, abline_valuesSuperM20, log_log_predictSuperM20, YSuperM20         = linear_regression(K,CasM20,K)
slopeSuperM40, interceptSuperM40, domainSuperM40, abline_valuesSuperM40, log_log_predictSuperM40, YSuperM40         = linear_regression(K,CasM40,K)
slopeSuperM100, interceptSuperM100, domainSuperM100, abline_valuesSuperM100, log_log_predictSuperM100, YSuperM100   = linear_regression(K,CasM100,K)

sigma_M4Sub = np.sqrt(sum((YSubM4 - log_log_predictSubM4)**2)/len(YSubM4))
sigma_M4Sup = np.sqrt(sum((YSuperM4 - log_log_predictSuperM4)**2)/len(YSuperM4))
sigma_M1    = np.sqrt(sum((YSuperM1 - log_log_predictSuperM1)**2)/len(YSuperM1))
sigma_M10   = np.sqrt(sum((YSuperM10 - log_log_predictSuperM10)**2)/len(YSuperM10))
sigma_M20   = np.sqrt(sum((YSuperM20 - log_log_predictSuperM20)**2)/len(YSuperM20))
sigma_M40   = np.sqrt(sum((YSuperM40 - log_log_predictSuperM40)**2)/len(YSuperM40))
sigma_M100  = np.sqrt(sum((YSuperM100 -log_log_predictSuperM100)**2)/len(YSuperM100))


# Combined plot

f, ax = plt.subplots(2,2,figsize=(14,9))
rc('font', **{'family': 'serif', 'serif': ['Computer Modern']})
rc('text', usetex=True)

xcoord = 0.80

ax[0,0].set_xscale("log", nonposx='clip')
ax[0,0].set_yscale("log", nonposy='clip')
ax[0,0].errorbar(WV_M1[0:470],M_M1[0:470],yerr=SD_M1[0:470],alpha=0.5,label='Mach 1')
ax[0,0].errorbar(WV_M4[0:5000],M_M4[0:5000],yerr=SD_M4[0:5000],color='blue',alpha=0.5,label='Mach 4')
ax[0,0].errorbar(WV_M10[0:470],M_M10[0:470],yerr=SD_M10[0:470],alpha=0.5,label='Mach 10')
ax[0,0].errorbar(WV_M20[0:470],M_M20[0:470],yerr=SD_M20[0:470],alpha=0.5,label='Mach 20')
ax[0,0].errorbar(WV_M40[0:470],M_M40[0:470],yerr=SD_M40[0:470],alpha=0.5,label='Mach 40')
ax[0,0].errorbar(WV_M100[0:470],M_M100[0:470],yerr=SD_M100[0:470],alpha=0.5,label='Mach 100')
ax[0,0].set_xlabel(r'$k$',size=16)
ax[0,0].set_ylabel(r'$\left\langle\mathcal{P}(k) 2 \pi k \right\rangle_{\theta}$',size=16)
ax[0,0].legend()


# Combined, compensated plot

ax[0,1].set_xscale("log", nonposx='clip')
ax[0,1].set_yscale("log", nonposy='clip')
ax[0,1].errorbar(WV_M10[0:470],M_M10[0:470]*WV_M10[0:470],yerr=SD_M10[0:470]*WV_M10[0:470],alpha=0.5,label='Mach 10')
ax[0,1].errorbar(WV_M20[0:470],M_M20[0:470]*WV_M20[0:470],yerr=SD_M20[0:470]*WV_M20[0:470],alpha=0.5,label='Mach 20')
ax[0,1].errorbar(WV_M40[0:470],M_M40[0:470]*WV_M40[0:470],yerr=SD_M40[0:470]*WV_M40[0:470],alpha=0.5,label='Mach 40')
ax[0,1].errorbar(WV_M100[0:470],M_M100[0:470]*WV_M100[0:470],yerr=SD_M100[0:470]*WV_M100[0:470],alpha=0.5,label='Mach 100')
ax[0,1].plot(np.array(K),np.array(CasM10)*np.array(K),'bo')
ax[0,1].plot(np.array(K),np.array(CasM20)*np.array(K),'yo')
ax[0,1].plot(np.array(K),np.array(CasM40)*np.array(K),'go')
ax[0,1].plot(np.array(K),np.array(CasM100)*np.array(K),'ro')
ax[0,1].annotate(r'$\mathcal{P}_{10} \propto k^{-1.73}$',xy=(0.05, 0.08),xycoords='axes fraction',fontsize=14,color='blue',rotation=-3)
ax[0,1].annotate(r'$\mathcal{P}_{20} \propto k^{-1.58}$',xy=(0.05, 0.16),xycoords='axes fraction',fontsize=14,color='orange',rotation=-3)
ax[0,1].annotate(r'$\mathcal{P}_{40} \propto k^{-1.49}$',xy=(0.05, 0.24),xycoords='axes fraction',fontsize=14,color='green',rotation=-3)
ax[0,1].annotate(r'$\mathcal{P}_{100} \propto k^{-1.33}$',xy=(0.05, 0.32),xycoords='axes fraction',fontsize=14,color='red',rotation=-3)
ax[0,1].set_xlabel(r'$k$',size=16)
ax[0,1].set_ylabel(r'$\left\langle\mathcal{P}(k) 2\pi k \right\rangle_{\theta} k$',size=16)
ax[0,1].legend()

# Mach 1 Plot
ax[1,0].set_xscale("log", nonposx='clip')
ax[1,0].set_yscale("log", nonposy='clip')
ax[1,0].errorbar(WV_M1[0:470],M_M1[0:470]*WV_M1[0:470]**2,yerr=SD_M1[0:470]*WV_M1[0:470]**2,alpha=0.5,label='Mach 1')
ax[1,0].set_xlabel(r'$k$',size=16)
ax[1,0].annotate(r'$\mathcal{P} \propto k^{-2.44}$',xy=(0.5, 0.85),xycoords='axes fraction',fontsize=14,color='blue',rotation=-3)
ax[1,0].plot(np.array(K),np.array(CasM1)*np.array(K)**2,'bo')
ax[1,0].set_ylabel(r'$\left\langle\mathcal{P}(k) 2 \pi k \right\rangle_{\theta} k^2$',size=16)
ax[1,0].legend()


# Mach 4 Plot
ax[1,1].set_xscale("log", nonposx='clip')
ax[1,1].set_yscale("log", nonposy='clip')
ax[1,1].errorbar(WV_M4[0:5000],M_M4[0:5000]*WV_M4[0:5000]**2,yerr=SD_M4[0:5000]*WV_M4[0:5000]**2,color='blue',alpha=0.5,label='Mach 4')
ax[1,1].plot(np.array(SubKM4),np.array(SubCasM4)*np.array(SubKM4)**2,'go')
ax[1,1].plot(np.array(SuperKM4),np.array(SuperCasM4)*np.array(SuperKM4)**2,'ro')
ax[1,1].set_xlabel(r'$k$',size=16)
ax[1,1].set_ylabel(r'$\left\langle\mathcal{P}(k) 2 \pi k \right\rangle_{\theta} k^2$',size=16)
ax[1,1].axvline(x=SonicScale,color='red',ls='--')
ax[1,1].axvline(x=UpperSubCascade,color='green',ls='--')
ax[1,1].axvline(x=LowerSubCascade,color='green',ls='--')
ax[1,1].axvline(x=UpperSuperCascade,color='purple',ls='--')
ax[1,1].axvline(x=LowerSuperCascade,color='purple',ls='--')
ax[1,1].axvline(x=SonicScale,color='red',ls='--')
ax[1,1].annotate(r'$\frac{L}{\lambda_s}$',xy=(10**1.7, 10**-3.5),fontsize=14,color='red')
ax[1,1].annotate(r'Subsonic Cascade',xy=(10**2.15, 10**-3.05),fontsize=12,color='green',rotation=90)
ax[1,1].annotate(r'Supersonic Cascade',xy=(10**1.05, 10**-3),fontsize=12,color='purple',rotation=90)
ax[1,1].annotate(r'$\mathcal{P} \propto k^{-2.20}$',xy=(10**2.15, 10**-2.3),fontsize=12,color='green')
ax[1,1].annotate(r'$\mathcal{P} \propto k^{-2.26}$',xy=(10**1.06, 10**-2.3),fontsize=12,color='purple')
ax[1,1].legend()


plt.tight_layout()
plt.show()
