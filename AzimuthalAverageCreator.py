''' This file takes in Mach 1, 4, 10, 20, 40, 100 time step data and
takes the azimuthal averaged power spectrum. It returns a .csv file of
each of the power spectrums.'''
import os;
import re;
import numpy as np;
import pandas as pd;                        # mathematics
import matplotlib.pyplot as plt;            # visualisation
import matplotlib.mlab as mlab;             # distribution viz
import argparse;                            # command line arguments
import imageio;                             # reading in mp4 data
import h5py;                                # importing in hdf5 files
import skimage;                             # import image data
import matplotlib.colors as colors;
from mpl_toolkits.axes_grid1 import make_axes_locatable;
from matplotlib import rc, ticker
from mpl_toolkits.axes_grid1.inset_locator import inset_axes
from scipy import fftpack

############################################################################################################################################
ap 			= argparse.ArgumentParser(description = 'Just a bunch of input arguments');
ap.add_argument('-type','--type',required=False,help='type argument: "2D" for a 2D visualisation "write" to write the azi averaged ps',type=str);
ap.add_argument('-numOfFiles','--numOfFiles',required=False,help='the number of files it iterate through from the text files created',type=int,default=1);
args 		= vars(ap.parse_args());
############################################################################################################################################


# Function Definitions
############################################################################################################################################

path = '/Volumes/JamesBe/6PanelFigure'; # YOUR PATH for writing -- CHANGE
os.chdir(path);

def line_reader(DataPath,MachNumber):
    MachPath = '/Volumes/JamesBe/' + DataPath # YOUR DATA PATH -- CHANGE
    os.system('ls ' + MachPath + '/EXTREME* ' + '>{}.txt'.format(MachNumber))
    lines = [line.rstrip('\n') for line in open('{}.txt'.format(MachNumber))]
    return lines

def h5py_reader(data):
    """
    This file reads the denisty projections in the hdf5
    file. It also ouputs the time, which is converted
    into turnover time later.

    """
    f            = h5py.File(data, 'r+');
    density      = np.array(f['dens_proj_xy'])
    time         = np.array(f['time']);
    f.close;
    return density, time

def time_reader(data):
    """
    This function just reads the time on a seprate occation.
    We need this to match times across simulation.
    """
    f            = h5py.File(data, 'r+');
    time         = np.array(f['time']);
    f.close;
    return time

def azimuthalAverage(image, center=None):
    """
    Calculate the azimuthally averaged radial profile.

    image - The 2D image
    center - The [x,y] pixel coordinates used as the center. The default is
             None, which then uses the center of the image (including
             fracitonal pixels).

    """
    # Calculate the indices from the image
    y, x = np.indices(image.shape)

    if not center:
        center = np.array([(x.max()-x.min())/2.0, (x.max()-x.min())/2.0])

    r = np.hypot(x - center[0], y - center[1])

    # Get sorted radii
    ind = np.argsort(r.flat)
    r_sorted = r.flat[ind]
    i_sorted = image.flat[ind]

    # Get the integer part of the radii (bin size = 1)
    r_int = r_sorted.astype(int)

    # Find all pixels that fall within each radial bin.
    deltar = r_int[1:] - r_int[:-1]  # Assumes all radii represented
    rind = np.where(deltar)[0]       # location of changed radius
    nr = rind[1:] - rind[:-1]        # number of radius bin

    # Cumulative sum to figure out sums for each radius bin
    csim = np.cumsum(i_sorted, dtype=float)
    tbin = csim[rind[1:]] - csim[rind[:-1]]

    radial_prof = tbin / nr
    return radial_prof

def FourierTransform(data,n,type):
    data    = ( 1/(n)**2)*fftpack.fft2(data)
    data    = fftpack.fftshift(data)
    data    = np.abs(data*np.conjugate(data))

    if type == 'aziAverage':
        data    = azimuthalAverage(data)

    return data

# Begin Script
##########################################################################################################

Mach1Files      = line_reader('Mach1_1024','Mach1');
Mach4Files      = line_reader('Mach4_10048','Mach4');
Mach10Files     = line_reader('Mach10_1024','Mach10');
Mach20Files     = line_reader('Mach20_1024','Mach20');
Mach40Files     = line_reader('Mach40_1024','Mach40');
Mach100Files    = line_reader('Mach100_1024','Mach100');


# Turnover Time
M1      = 1;
M4      = 4;
M10     = 10;
M20     = 20;
M40     = 40;
M100    = 100;

# Fourier transform state: either azi or 2D power spectrum
state   = args['type'];


file_counter    = 0;
Mach4_counter   = 1;
numberOfFiles   = args['numOfFiles'];

for i in xrange(700,700+numberOfFiles):

    # Read in files
    Mach1, M1Time       = h5py_reader(Mach1Files[i]);
    Mach10, M10Time     = h5py_reader(Mach10Files[i]);
    Mach20, M20Time     = h5py_reader(Mach20Files[i]);
    Mach40, M40Time     = h5py_reader(Mach40Files[i]);
    Mach100, M100Time   = h5py_reader(Mach100Files[i]);

    if file_counter == 0:
        Mach4, M4Time     = h5py_reader(Mach4Files[150]);
        ToM4              = M4Time*M4*2;

    # All turnover times
    ToM1    = M1Time*M1*2
    ToM10   = M10Time*M10*2
    ToM20   = M20Time*M20*2
    ToM40   = M40Time*M40*2
    ToM100  = M100Time*M100*2

    if ToM1[0] >= ToM4[0]:
        try:

            M4Time     = time_reader(Mach4Files[Mach4_counter])
            ToM4       = M4Time*M4*2

            while ToM1[0] > ToM4[0]:
                M4Time          = time_reader(Mach4Files[Mach4_counter])
                ToM4            = M4Time*M4*2
                Mach4_counter   += 1

            Mach4, M4Time   = h5py_reader(Mach4Files[Mach4_counter])

        except IndexError:
            Mach4, M4Time   = h5py_reader(Mach4Files[339])

    # azimuthally averaged Fourier spectrum
    M1_psd2D    = FourierTransform(Mach1-1,1024.0,state)
    M4_psd2D    = FourierTransform(Mach4-1,10048.0,state)
    M10_psd2D   = FourierTransform(Mach10-1,1024.0,state)
    M20_psd2D   = FourierTransform(Mach20-1,1024.0,state)
    M40_psd2D   = FourierTransform(Mach40-1,1024.0,state)
    M100_psd2D  = FourierTransform(Mach100-1,1024.0,state)

    file_counter += 1

    if file_counter == 1:
        Mach1_data      = M1_psd2D
        Mach4_data      = M4_psd2D
        Mach10_data     = M10_psd2D
        Mach20_data     = M20_psd2D
        Mach40_data     = M40_psd2D
        Mach100_data    = M100_psd2D
    else:
        Mach1_data      = np.vstack((Mach1_data,M1_psd2D))
        Mach4_data      = np.vstack((Mach4_data,M4_psd2D))
        Mach10_data     = np.vstack((Mach10_data,M10_psd2D))
        Mach20_data     = np.vstack((Mach20_data,M20_psd2D))
        Mach40_data     = np.vstack((Mach40_data,M40_psd2D))
        Mach100_data    = np.vstack((Mach100_data,M100_psd2D))


    print('Iteration number {}'.format(file_counter) + ' done.')

    if args['type'] == 'write':

        if np.mod(file_counter,5) == 0:
            df1 = pd.DataFrame(Mach1_data)
            df1.to_csv("Mach1.csv")

            df4 = pd.DataFrame(Mach4_data)
            df4.to_csv("Mach4.csv")

            df10 = pd.DataFrame(Mach10_data)
            df10.to_csv("Mach10.csv")

            df20 = pd.DataFrame(Mach20_data)
            df20.to_csv("Mach20.csv")

            df40 = pd.DataFrame(Mach40_data)
            df40.to_csv("Mach40.csv")

            df100 = pd.DataFrame(Mach100_data)
            df100.to_csv("Mach100.csv")
        elif file_counter == 799:
            df1 = pd.DataFrame(Mach1_data)
            df1.to_csv("Mach1.csv")

            df4 = pd.DataFrame(Mach4_data)
            df4.to_csv("Mach4.csv")

            df10 = pd.DataFrame(Mach10_data)
            df10.to_csv("Mach10.csv")

            df20 = pd.DataFrame(Mach20_data)
            df20.to_csv("Mach20.csv")

            df40 = pd.DataFrame(Mach40_data)
            df40.to_csv("Mach40.csv")

            df100 = pd.DataFrame(Mach100_data)
            df100.to_csv("Mach100.csv")
        elif file_counter == 800:
            df1 = pd.DataFrame(Mach1_data)
            df1.to_csv("Mach1.csv")

            df4 = pd.DataFrame(Mach4_data)
            df4.to_csv("Mach4.csv")

            df10 = pd.DataFrame(Mach10_data)
            df10.to_csv("Mach10.csv")

            df20 = pd.DataFrame(Mach20_data)
            df20.to_csv("Mach20.csv")

            df40 = pd.DataFrame(Mach40_data)
            df40.to_csv("Mach40.csv")

            df100 = pd.DataFrame(Mach100_data)
            df100.to_csv("Mach100.csv")

    elif args['type'] == '2D':

        if numberOfFiles != 1:
            print 'You have more than a single file, so now I will break to avoid making many plots.'
            break

        f, ax = plt.subplots(3, 2, figsize=(8.78, 10.5), dpi=400)


        f.subplots_adjust(wspace=0.01,hspace=0.01)#,left=0, bottom=0, right=1, top=1)
        fs = 16;

        rc('font', **{'family': 'serif', 'serif': ['Computer Modern']})
        rc('text', usetex=True)

        vmax = -1;
        vmin = -23;


        im1 = ax[0,0].imshow( np.log10( M1_psd2D ), cmap=plt.cm.plasma,vmin = vmin, vmax = vmax)#,vmin=np.log10(Mach4.min()/Mach4.mean()), vmax=np.log10(Mach4.max()/Mach4.mean()))
        ax[0,0].annotate(r'$\mathcal{M} \approx 1$, $t = $' + str(round(ToM1[0],1)) + r'$T$',
                         xy=(0.02, 0.9),xycoords='axes fraction',
                         xytext=(0.02, 0.9),textcoords='axes fraction',
                         fontsize=fs,color='w')
        ax[0,0].set_xticks([])
        ax[0,0].set_yticks([])


        # Mach 4 Plot
        im2 = ax[0,1].imshow( np.log10(M4_psd2D), cmap=plt.cm.plasma, vmin = vmin, vmax = vmax)
        ax[0,1].annotate(r'$\mathcal{M} \approx 4$, $t = $' + str(round(ToM4[0],1)) + r'$T$',
                         xy=(0.02, 0.9),xycoords='axes fraction',
                         xytext=(0.02, 0.9),textcoords='axes fraction',
                         fontsize=fs,color='w')
        ax[0,1].set_xticks([])
        ax[0,1].set_yticks([])


        # Mach 10 Plot
        im3 = ax[1,0].imshow( np.log10(M10_psd2D), cmap=plt.cm.plasma, vmin = vmin, vmax = vmax)
        ax[1,0].annotate(r'$\mathcal{M} \approx 10$, $t = $' + str(round(ToM10[0],1)) + r'$T$',
                         xy=(0.02, 0.9),xycoords='axes fraction',
                         xytext=(0.02, 0.9),textcoords='axes fraction',
                         fontsize=fs,color='w')
        ax[1,0].set_xticks([])
        ax[1,0].set_yticks([])

        # Mach 20 Plot
        im4 = ax[1,1].imshow( np.log10(M20_psd2D), cmap=plt.cm.plasma, vmin = vmin, vmax= vmax)
        ax[1,1].annotate(r'$\mathcal{M} \approx 20$, $t = $' + str(round(ToM20[0],1)) + r'$T$',
                         xy=(0.02, 0.9),xycoords='axes fraction',
                         xytext=(0.02, 0.9),textcoords='axes fraction',
                         fontsize=fs,color='w')
        ax[1,1].set_xticks([])
        ax[1,1].set_yticks([])

        # Mach 40 Plot
        im5 = ax[2,0].imshow( np.log10(M40_psd2D ), cmap=plt.cm.plasma, vmin = vmin, vmax = vmax)
        ax[2,0].annotate(r'$\mathcal{M} \approx 40$, $t = $' + str(round(ToM40[0],1)) + r'$T$',
                         xy=(0.02, 0.9),xycoords='axes fraction',
                         xytext=(0.02, 0.9),textcoords='axes fraction',
                         fontsize=fs,color='w')
        ax[2,0].set_xticks([])
        ax[2,0].set_yticks([])

        #Mach 100
        im6 = ax[2,1].imshow(np.log10( M100_psd2D/100**2 ), cmap=plt.cm.plasma, vmin = vmin, vmax = vmax)
        ax[2,1].annotate(r'$\mathcal{M} \approx 100$, $t = $' + str(round(ToM100[0],1)) + r'$T$',
                         xy=(0.02, 0.9),xycoords='axes fraction',
                         xytext=(0.02, 0.9),textcoords='axes fraction',
                         fontsize=fs,color='w')
        ax[2,1].set_xticks([])
        ax[2,1].set_yticks([])



        # Colour bars
        ####################################################################################################################################
        tick_locator = ticker.MaxNLocator(nbins=11)
        cbar3 = f.colorbar(im1,ax=ax.ravel().tolist(),format='%.2f') #,cax=axins1
        cbar3.set_label(r'$\log_{10}\left( \mathcal{P}(k,t) \right)$',fontsize=16,rotation=270,labelpad=25)
        cbar3.locator = tick_locator
        cbar3.update_ticks()

        #plt.tight_layout()
        plt.savefig('PowerSpectrum{}.png'.format(file_counter),dpi=400)
        plt.close()
